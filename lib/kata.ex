import :math
defmodule Kata do
  def area_of_polygon_inside_circle(r, s) do
    (1 / 2) * s * pow(r, 2) * sin(2 * pi() / s) |> Float.round(3)
  end
end